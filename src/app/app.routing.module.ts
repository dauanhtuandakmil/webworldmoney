import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
const routes: Routes = [

    {path: '', component: AppComponent, children: [   
          {path: 'currency', loadChildren: './currency/currency.module#CurrencyModule'},
            {path: 'sellandbuy', loadChildren: './sellandbuy/sellandbuy.module#SellandbuyModule'},
            {path: '**', redirectTo:'currency'}
    ]}

 
  
]
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }