import { Component ,OnInit} from '@angular/core';
import { CurrencyService } from './core/Service/currency.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Web-Money';
  currencies = [];
  constructor(
    public currencyserivice: CurrencyService
  ){

  }
}
