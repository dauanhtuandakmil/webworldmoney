import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellandbuyComponent } from './sellandbuy.component';

describe('SellandbuyComponent', () => {
  let component: SellandbuyComponent;
  let fixture: ComponentFixture<SellandbuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellandbuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellandbuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
