import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SellandbuyComponent } from './sellandbuy.component';
@NgModule({
    declarations: [
            SellandbuyComponent
      ],
      imports: [
        FormsModule,
        CommonModule
      ],
      exports: [
        SellandbuyComponent
      ],
      providers: [],
 })
 export class SellandbuyModule{}
