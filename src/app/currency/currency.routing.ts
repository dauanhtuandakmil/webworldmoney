import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellandbuyComponent } from '../sellandbuy/sellandbuy.component';
import { CaculatorComponent } from '../caculator/caculator.component';
import { CurrencyComponent } from './currency.component';

const routes: Routes = [
    {path:'currency', component: CurrencyComponent},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})

export class CurrencyRoutingModule{}