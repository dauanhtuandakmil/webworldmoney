import { NgModule } from '@angular/core';
import { CurrencyRoutingModule } from './currency.routing';
import { CurrencyComponent } from './currency.component';
import { ChartsModule } from 'ng2-charts';
import { RouterModule } from '@angular/router';
import { SellandbuyComponent } from '../sellandbuy/sellandbuy.component';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import {MyDatePickerModule} from 'mydatepicker';
@NgModule({
    declarations: [
      CurrencyComponent,
    ],
    imports: [
      CurrencyRoutingModule,
      ChartsModule,
      RouterModule,
      CommonModule,
      FormsModule,
      DlDateTimeDateModule,  // <--- Determines the data type of the model
      DlDateTimePickerModule,
      MyDatePickerModule
    ],
    exports: [
    CurrencyComponent
    ],
    providers: [],
  })
  export class CurrencyModule{}