import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { CurrencyService } from '../core/Service/currency.service';
import { TygiaService } from '../core/Service/Tygia.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit, OnDestroy {
  isShowChart = 0;
  currencies = [];
  tygia1 = [];
  tygia2 = [];
  // currencies = {
  //   USD : [
  //     {data:[17171.26, 17171.26,17171.26,17194.49], label:'USD'}
  //   ],
  //   VND : [
  //     {data:[17171.26, 17171.26,17171.26,17194.49],label:'VND'}
  //   ]
  // }
  charOption = {
    responsive: true
  };
  subs: Subscription;
  public myDatePickerOptions: IMyDpOptions = {

    dateFormat: 'dd.mm.yyyy',
  };
  public myForm: FormGroup;
  public model: any = { date: { year: 2018, month: 10, day: 9 } };
  constructor(
    public currenyservice: CurrencyService,
    public tygiaService: TygiaService,
    private router: Router,
    private ren2: Renderer2,
    private formbuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.subs = this.currenyservice.getCurrency().subscribe(data => {
      this.currencies = data;
      console.log('du lieu', data);
    })
    this.myForm = this.formbuilder.group({
      // Empty string or null means no initial value. Can be also specific date for
      // example: {date: {year: 2018, month: 10, day: 9}} which sets this date to initial
      // value.

      myDate: [null, Validators.required]
      // other controls are here...
    });
  }
  setDate(): void {
    // Set today date using the patchValue function
    let date = new Date();
    this.myForm.patchValue({
      myDate: {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        }
      }
    });
  }
  clearDate(): void {
    // Clear the date using the patchValue function
    this.myForm.patchValue({ myDate: null });
  }
  onDateChanged(event: IMyDateModel) {
    // event properties are: event.date, event.jsdate, event.formatted and event.epoc
  }
  chartData = [
    { data: [], label: '' },
    { data: [], label: '' },
  ];
  chartLabels = [];
  onChartClick(event) {
    console.log(event);
  }
  chartHover(event) {
    console.log(event);

  }
  randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    const clone = JSON.parse(JSON.stringify(this.chartData));
    clone[0].data = data;
    this.chartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }
  xem(val1, val2) {
    this.isShowChart = 0;
    console.log('ng1', val1);
    console.log('ng2', val2);
    let su1, su2;
    su1 = this.tygiaService.getTygia("ma=" + val1).subscribe(res => {
      // for (let idx = 0; idx < res.tygia.length; idx++) {
      //   const element = res.tygia[idx];
      //   this.chartData[0].data.push(element.heso);
      // }
      res.tygia.forEach(element => {
        this.chartData[0].data.push(element.heso);
      });
      console.log(this.chartData[0].data);
      this.isShowChart++;
      su1.unsubscribe();
    });
    su2 = this.tygiaService.getTygia("ma=" + val2).subscribe(res => {
      // for (let idx = 0; idx < res.tygia.length; idx++) {
      //   const element = res.tygia[idx];
      //   this.chartData[1].data.push(element.heso);
      // }
      res.tygia.forEach(element => {
        this.chartData[1].data.push(element.heso);
        this.chartLabels.push(element.ngay);
      });
      console.log(this.chartData[1].data);

      this.isShowChart++;
      su2.unsubscribe();
    });
  }
  sellandbuy() {
    console.log('click');

    this.router.navigate(['/sellandbuy']);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
  dangnhap(){
    
  }
}
