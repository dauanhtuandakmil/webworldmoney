import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CurrencyComponent } from './currency/currency.component';
import { SellandbuyComponent } from './sellandbuy/sellandbuy.component';
import { CaculatorComponent } from './caculator/caculator.component';
import { CurrencyModule } from './currency/currency.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { SellandbuyModule } from './sellandbuy/sellandbuy.module';
import { AppRoutingModule } from './app.routing.module';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MatDatepickerModule, MatInputModule, MatNativeDateModule}  from '@angular/material';
import { AdminComponent } from './admin/admin.component';

@NgModule({
  declarations: [
    AppComponent,
    CaculatorComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    CoreModule.forRoot(),
    CurrencyModule,
    Ng2SearchPipeModule,
    FormsModule,
    SellandbuyModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
