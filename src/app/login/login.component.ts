import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../core/Service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  constructor(
    private fb: FormBuilder, 
    public loginservice: LoginService,
    public router: Router
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  dangnhap(){
    console.log('abc', this.form.value);
    const body = {
      tenDangNhap: this.form.value.username,
      matKhau: this.form.value.password
    }
    
    this.loginservice.login(body).subscribe(res =>{
      this.router.navigateByUrl('/currency');
    })
    
  }

}
