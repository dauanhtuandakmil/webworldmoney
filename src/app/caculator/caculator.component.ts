import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CurrencyService } from '../core/Service/currency.service';
import { TygiaService } from '../core/Service/Tygia.service';
@Component({
  selector: 'app-caculator',
  templateUrl: './caculator.component.html',
  styleUrls: ['./caculator.component.scss']
})
export class CaculatorComponent implements OnInit {
  public form: FormGroup
  ketqua;
  images: {}
  currencies =[];
  tygia = [];
  params ={
    ma:'USD'
  }
  constructor(
    private fb: FormBuilder,
    public currencyprovider: CurrencyService,
    public Tygia: TygiaService
  ) { 
    this.Tygia.getTygia(this.params).subscribe(data =>{
      this.tygia = data;
      console.log(this.tygia);
      
    })
  }
  ngOnInit() {
    this.form = this.fb.group({
      sotiennhap: [''],
      ketqua: ['']
    })
    this.currencyprovider.getCurrency().subscribe(data=>{
      this.currencies = data;
    });
   
  }
  change(val){
    console.log('val1',val);
    let su;
   this.Tygia.getTygia("ma="+val).subscribe(data=>{
      
      data.tygia.forEach(element => {
        console.log(element.heso);
        
        this.form.value.ketqua = this.form.value.sotiennhap * element.heso;
        this.images = element.img;
      });
      this.ketqua = this.form.value.ketqua; 
    })
  
  }
  changeMoney(){

  }

}
