import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderInterceptor } from './Service/http-intercepter';
import { CurrencyService } from './Service/currency.service';
import { TygiaService } from './Service/Tygia.service';
import { LoginService } from './Service/login.service';
@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  exports: [],
  providers: [],
})
export class CoreModule { 
  static forRoot(): ModuleWithProviders {
    return {
        ngModule: CoreModule,
        providers: [
          CurrencyService,
          TygiaService,
          LoginService,
          {
            provide: HTTP_INTERCEPTORS,
            useClass: HeaderInterceptor,
            multi: true
          },
        ]
    };
}
}
