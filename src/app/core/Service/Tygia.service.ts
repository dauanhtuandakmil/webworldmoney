import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable()
export class TygiaService {
    constructor(public http: HttpClient){

    }
    getTygia(params):Observable<any>{
        return this.http.get('/api/tygia?'+params);
    }
}